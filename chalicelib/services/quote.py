import json
import random
from pathlib import Path


quotes_file = Path('chalicelib/data/quotes.json')
QUOTES = json.loads(quotes_file.read_text())


class QuoteService():
    def new(self):
        # get random quote, extract author
        author, quote = random.choice(QUOTES).split(': ')
        return {'author': author, 'quote': quote}
