# beepboop

Named after the sounds a (fictional) robot makes, `BeepBoop` is a Serverless App that returns a random quote from the award-winning series [Mr. Robot](https://www.imdb.com/title/tt4158110/).

BeepBoop demonstates a Serverless App framework built on top of the OpenSource micro-framework [Chalice](https://aws.github.io/chalice) that takes off most, if not all, pain points while building and managing a Serverless application. Chalice is built by the AWS team for building and deploying Serverless apps on AWS.

This project can be used as a starting point(scaffold) to write your own Serveless application.

## Install
`make install`

## Test
`make test`

## Run
1. `make run`
2. `curl http://127.0.0.1:8000/quote`

## Deploy
1. Ensure AWS config and credentials with right perimissions are set in your machine
2. Set Lambda stage: (optional, default=dev) `export ENV=dev`
3. `make deploy`

## Implementation notes
This project is an adapted version of a Serverless application we use in production. However, the important implementation ideas are kept intact here for maximum benefit.

### Project structure
```
.
├── app.py                <-- application entrypoint
├── .chalice              <-- chalice deployment config
│   └── config.json
├── chalicelib            <-- custom code location
│   ├── data              <-- raw data files
│   │   └── quotes.json
│   ├── __init__.py
│   └── services          <-- custom classes to abstract app entities
│       └── quote.py
├── Makefile              <-- orchestration via Makefile
├── Pipfile               <-- virtualenv management
├── Pipfile.lock
├── README.md             <-- documentation
├── requirements.dev.txt  <-- dev dependencies, chalice ignores this file
├── requirements.txt      <-- prod dependencies, chalice automatically installs these
└── tests                 <-- tests
    ├── __init__.py
    └── test_routes.py
```

## Author
[Arabinda Dora](https://in.linkedin.com/in/akdora)
