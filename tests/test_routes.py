from chalice.test import Client

import app


def test_ping_route():
    with Client(app.app) as client:
        response = client.http.get('/ping')
        assert response.status_code == 200
        assert response.json_body == {'message': 'pong'}


def test_quote_route():
    with Client(app.app) as client:
        response = client.http.get('/quote')
        assert response.status_code == 200
        assert 'quote' in response.json_body
        assert 'author' in response.json_body
