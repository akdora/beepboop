ENV ?= dev

# export variables to child processes
# deploy lambda to this region
deploy: export AWS_DEFAULT_REGION = eu-west-1
delete: export AWS_DEFAULT_REGION = eu-west-1

help:
	echo 'Usage:'
	echo '  make <target>'
	echo ''
	echo 'Targets:'
	echo '  install  Setup this project'
	echo '  run      Run Lambda locally'
	echo '  test     Run unit tests'
	echo '  deploy   Deploy the Lambda'
	echo '  delete   Delete deployment'
	echo '  clean    Delete old deployment packages'

install:
	pip install pipenv
	pipenv --python python3.8
	pipenv install -r requirements.txt
	pipenv install --dev -r requirements.dev.txt

run:
	chalice local

test:
	pytest tests

deploy:
	chalice deploy --stage $(ENV) --connection-timeout 360

delete:
	chalice delete --stage $(ENV)

clean:
	rm -rf .chalice/deployments


.SILENT: help
.DEFAULT_GOAL := help
.PHONY: help install run test deploy delete clean
