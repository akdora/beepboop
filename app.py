from chalice import Chalice
from chalicelib import QUOTE_SERVICE


app = Chalice(app_name='beepboop')


@app.route('/ping')
def ping():
    return {'message': 'pong'}


@app.route('/quote')
def quote():
    return QUOTE_SERVICE.new()
